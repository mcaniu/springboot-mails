package com;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.configuration")
@ComponentScan("com.services")
public class StartApplication implements CommandLineRunner {

//    @Autowired
//    private MailService mailService;

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }

    @Override
    public void run(String... arg) {
        /*try {
            MailDTO mailDTO = MailDTO.builder().to("mcaniu.apiux@supereduc.cl").subject("es un asunro").text("esdte es un body?").build();
            // envio de un correo simple
            mailService.sendSimpleMessage(mailDTO);

            // envio de un correo con plantilla thymeleaf
            Map<String, Object> templateModel = new HashMap<>();
            templateModel.put("recipientName", "Hola 2  con acentó asi lala1");
            templateModel.put("text", "Hola 2  con acentó asi lalal2");
            templateModel.put("senderName", "Hola 2  con acentó asi lala3");
            mailService.sendMessageUsingThymeleafTemplate(mailDTO);

            // envio de un correo con archivo adjunto
            mailService.sendMessageWithAttachment(mailDTO);

        } catch (IOException | MessagingException e) {
            e.printStackTrace();
        }*/

    }
}
