package com.services.impl;

import com.dtos.MailDTO;
import com.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SimpleMailMessage template;

    @Autowired
    private SpringTemplateEngine thymeleafTemplateEngine;

    @Value("classpath:/spring-boot-logo.png")
    private Resource resourceFile;

    @Override
    public void sendSimpleMessage(MailDTO mailDTO) {
        String text = String.format(template.getText(), mailDTO.getText());

        try {
            SimpleMailMessage message = new SimpleMailMessage();
            // message.setFrom("noreply@gmail.com");
            message.setTo(mailDTO.getTo());
            message.setSubject(mailDTO.getSubject());
            message.setText(text);

            emailSender.send(message);
        } catch (MailException exception) {
            exception.printStackTrace();
        }
    }


    @Override
    public void sendMessageUsingThymeleafTemplateWithAttachment(MailDTO mailDTO) throws MessagingException {
        Map<String, Object> camposPlantilla = new HashMap<>();
        camposPlantilla.put("body", mailDTO.getText());

        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(camposPlantilla);

        String htmlBody = thymeleafTemplateEngine.process("plantilla-thymeleaf.html", thymeleafContext);

        // procesar html generado
        MimeMessage message = emailSender.createMimeMessage();


        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        // helper.setFrom("noreply@gmail.com");
        helper.setTo(mailDTO.getTo());
        helper.setSubject(mailDTO.getSubject());
        helper.setText(htmlBody, true);

        helper.addInline("logo.png", resourceFile);


        if (mailDTO.getFiles() != null) {
            for (MultipartFile attachment : mailDTO.getFiles()) {
                // Attach each file to the email
                if (attachment.getOriginalFilename() != null) {
                    helper.addAttachment(attachment.getOriginalFilename(), attachment::getInputStream);
                }

            }
        }


        emailSender.send(message);
    }


}
