package com.services;

import com.dtos.MailDTO;

import javax.mail.MessagingException;

public interface MailService {

    /**
     * Metodo que envia un correo simple
     *
     * @param mailDTO to correo hacia donde va, subject asunto, templateModel cuerpo del mensaje.
     */
    void sendSimpleMessage(MailDTO mailDTO);


    /**
     * Metodo que envia un correo usando una plantilla de thymeleaf con o sin archivos
     *
     * @param mailDTO to correo hacia donde va, subject asunto, templateModel cuerpo del mensaje, files archivos adjuntos.
     */
    void sendMessageUsingThymeleafTemplateWithAttachment(MailDTO mailDTO) throws MessagingException;

}
