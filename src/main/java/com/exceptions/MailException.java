package com.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailException extends Exception {

    private String message;

    public MailException(String message) {
        super();
        this.message = message;
    }
}
