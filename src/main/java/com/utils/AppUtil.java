package com.utils;

import com.exceptions.MailException;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

public class AppUtil {

    public static void validarCamposBases(String to, String subject, String text) throws MailException {
        List<String> errores = new ArrayList<>();

        boolean toEsNulaoVacia = Strings.isNullOrEmpty(to);
        boolean subjectEsNulaoVacia = Strings.isNullOrEmpty(subject);
        boolean textEsNulaoVacia = Strings.isNullOrEmpty(text);

        if (toEsNulaoVacia) {
            errores.add("to");
        }

        if (subjectEsNulaoVacia) {
            errores.add("subject");
        }

        if (textEsNulaoVacia) {
            errores.add("text");
        }

        if (errores.size() > 0) {
            String glosa = "Los campos no deben ir vacios: ".concat(String.join(",", errores));
            throw new MailException(glosa);
        }

    }
}
