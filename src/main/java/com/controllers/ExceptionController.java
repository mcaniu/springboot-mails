package com.controllers;

import com.dtos.ResponseDTO;
import com.exceptions.MailException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@Slf4j
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseDTO> handleException(Exception ex) {
        log.error("Error=" + ex);

        String details = ex.toString();
        String message = "Error interno";
        if (ex instanceof MailException) {
            details = ex.getMessage();
            message = "Validación";
        }

        ResponseDTO error = ResponseDTO.builder().details(details).message(message).timestamp(new Date()).build();
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
