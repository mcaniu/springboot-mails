package com.controllers;


import com.dtos.MailDTO;
import com.dtos.ResponseDTO;
import com.exceptions.MailException;
import com.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static com.utils.AppUtil.validarCamposBases;

@RestController
@RequestMapping("/v1/mails")
public class MailController {

    @Autowired
    private MailService mailService;

    @PostMapping("/send")
    public ResponseEntity<?> send(@RequestParam("to") String to,
                                  @RequestParam("subject") String subject,
                                  @RequestParam("text") String text,
                                  @RequestPart(value = "files", required = false) List<MultipartFile> files) throws MailException, IOException, MessagingException {

        // validacion campos basicos
        validarCamposBases(to, subject, text);


        // armado de objeto usado en las clases services
        MailDTO mailDTO = MailDTO.builder().to(to).subject(subject).text(text).files(files).build();

        // envio de correos simple
        // mailService.sendSimpleMessage(mailDTO);

        // envio de correos con plantilla con o sin archivos adjuntados
        mailService.sendMessageUsingThymeleafTemplateWithAttachment(mailDTO);

        ResponseDTO response = ResponseDTO.builder().details("OK").message("OK").timestamp(new Date()).build();
        return ResponseEntity.ok(response);
    }


}
