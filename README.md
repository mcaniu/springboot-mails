# springboot mails
## Proyecto plantilla para envio de correos
Proyecto maqueta o plantilla para generar un rest api para envio de correos relizada con gradle, a las que se les hicieron ciertas modificaciones especificadas en
el changelog.

Referencia del Ejemplo
Fuente Web: https://www.baeldung.com/spring-email-templates
Repositorio: https://github.com/eugenp/tutorials/blob/master/spring-web-modules/spring-mvc-basics-2/src/main/java/com/baeldung/spring/controller/MailController.java

## Futuras mejoras
- ejemplo con outlook
- pruebas unitarias

## Tecnologias aplicadas
- java 1.8
- springboot v2.7.3
- gradle-6.8